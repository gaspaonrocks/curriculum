const { join, resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const optimization = {
  splitChunks: {
    chunks: 'async',
    minSize: 30000,
    maxSize: 35000,
    minChunks: 1,
    maxAsyncRequests: 5,
    maxInitialRequests: 3,
    automaticNameDelimiter: '~',
    name: true,
    cacheGroups: {
      vendors: {
        test: /[\\/]node_modules[\\/]/,
        priority: -10,
        chunks: 'all',
      },
      default: {
        minChunks: 2,
        priority: -20,
        reuseExistingChunk: true,
      },
    },
  },
};

module.exports = {
  serve: {
    content: join(__dirname, 'dist'),
    port: 3000,
  },
  devtool: 'source-map',
  mode: 'production',
  entry: join(resolve(__dirname, 'src'), 'index.js'),
  output: {
    path: join(__dirname, 'dist'),
    filename: 'app.bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: ['eslint-loader', 'babel-loader'],
      },
      {
        test: /\.s?css$/,
        exclude: /(node_modules)/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: join(resolve(__dirname, 'src'), 'index.html'),
    }),
  ],
  optimization: optimization,
};
