# Curriculum Vitae

A nice way to showcase what I have learned.

# To Do
 - [ ] Add a section for ~~education~~, experience, hobbies...
 - [ ] find a nice way to showcase the others 'projects'
 - [ ] add a footer to let people contact me.

# Done
 - [X] Add routing with `react-router`
 - [X] added [`direction-reveal`](https://nigelotoole.github.io/direction-reveal/) component. Needs tweaking...

_To Be Continued..._
