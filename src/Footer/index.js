import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class Footer extends Component {
  render() {
    return (
      <section class="hero">
        <div class="hero-body">
          <div class="columns">
            <div class="column">
              <FontAwesomeIcon icon="home" />
            </div>
            <div class="column">
              <FontAwesomeIcon icon={["fab", "github"]} />
            </div>
            <div class="column">
              <FontAwesomeIcon icon="mail-bulk" />
            </div>
            <div class="column">
              Fourth column
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default Footer;
