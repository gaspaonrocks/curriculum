import React, { Component } from 'react';

import './styles.scss';

import DirectionReveal from '../../lib/direction-reveal/scripts/direction-reveal';

import Main from '../Main';
import Footer from '../Footer';
import Header from '../Header';

class App extends Component {
  componentDidMount() {
    // Init with all options at default setting
    DirectionReveal({
      selector: '.direction-reveal', // Container element selector.
      itemSelector: '.direction-reveal__card', // Item element selector.
      animationName: 'swing', // Animation CSS class.
      enableTouch: true, // Adds touch event to show content on first click then follow link on the second click.
      touchThreshold: 250, // Touch length must be less than this to trigger reveal which prevents the event triggering if user is scrolling.
    });
  }
  render() {
    return (
      <div>
        <Header />
        <Main />
      </div>
    );
  }
}

export default App;
