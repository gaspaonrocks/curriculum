import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Education from '../Education';
import Experience from '../Experience';

class Main extends Component {
  render() {
    return (
      <div>
        <Route exact path="/education" component={Education} />
        <Route exact path="/experience" component={Experience} />
      </div>
    )
  }
}

export default Main;
