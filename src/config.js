import { library } from '@fortawesome/fontawesome-svg-core';
import { faHome, faMailBulk } from '@fortawesome/free-solid-svg-icons';
import { faGithub } from '@fortawesome/free-brands-svg-icons';

export default () => { library.add(faHome, faMailBulk, faGithub) };
