import React, { Component, Fragment } from 'react';
import { NavLink } from 'react-router-dom'

import './styles.scss';

class Header extends Component {
  render() {
    return (
      <Fragment>
        <nav className="nav-container" role="navigation" aria-label="main navigation">
          <a href="#">
            <img
              className="img-logo"
              src="https://logos-download.com/wp-content/uploads/2016/09/React_logo_wordmark.png"
              alt="React Logo"
            />
          </a>
          <div className="nav-list">
            <NavLink className="nav-item" to='/' tabIndex={0}>Home</NavLink>
            <NavLink className="nav-item" to='/education' tabIndex={0}>Education</NavLink>
            <NavLink className="nav-item" to='/experience' tabIndex={0}>Experience</NavLink>
            <NavLink className="nav-item" to='/interests' tabIndex={0}>Interests</NavLink>
          </div>
        </nav>
      </Fragment>
    );
  }
}

export default Header;
