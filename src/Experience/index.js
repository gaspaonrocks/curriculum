import React, { PureComponent } from 'react';

import './styles.scss';
import DisplayCard from '../DisplayCard';

class Experience extends PureComponent {
  render() {
    const attr = {
      title: 'Company',
      description: 'Bloc de Description',
      imageAlt: 'blablabla'
    }

    return (
      <div className="experience-container">
        <DisplayCard title={attr.title} description={attr.description} imageAlt={attr.imageAlt} animationName='flip' />
        <DisplayCard title={attr.title} description={attr.description} imageAlt={attr.imageAlt} animationName='flip' />
        <DisplayCard title={attr.title} description={attr.description} imageAlt={attr.imageAlt} animationName='flip' />
      </div>
    );
  }
}

export default Experience;
