import React, { Component } from 'react';

import './styles.scss';
import DirectionReveal from '../../lib/direction-reveal/scripts/direction-reveal';

/**
 * Direction-Reveal Component
 * Take the same parameters in entry, use them as props
 * Use React Events directly 'onMouseEnter/onMouseLeave', 'onFocus/onBlur'
 * Use classNames instead of regular classes.
 */

class RevealTile extends Component {
  componentDidMount() {
    // Init with all options at default setting
    DirectionReveal({
      selector: '.direction-reveal', // Container element selector.
      itemSelector: '.direction-reveal__card', // Item element selector.
      animationName: this.props.animationName, // Animation CSS class.
      enableTouch: true, // Adds touch event to show content on first click then follow link on the second click.
      touchThreshold: 250, // Touch length must be less than this to trigger reveal which prevents the event triggering if user is scrolling.
    });
  }

  render() {
    const animatedImg = this.props.animationName === 'flip';
    const imgClasses = animatedImg ? 'direction-reveal__anim--out img-fluid' : 'img-fluid';

    return (
      <div className="direction-reveal">
        <div className="direction-reveal__card">
          <img src={this.props.imageSrc} alt={this.props.imageAlt} className={imgClasses} />

          <div className="direction-reveal__overlay direction-reveal__anim--in">
            <h3 className="direction-reveal__title">{this.props.title}</h3>
            <p className="direction-reveal__text">{this.props.description}</p>
          </div>
        </div>
      </div>
    );
  }
}

RevealTile.defaultProps = {
  imageSrc: 'http://kensap.org/wp-content/uploads/empty-photo.jpg',
  imageAlt: 'emptyImage',
  title: 'Empty Title',
  description: 'Empty Description.',
};

export default RevealTile;
