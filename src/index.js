import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom'

import App from './App';
import config from './config';

config();

const entryNode = document.getElementById('app');
ReactDOM.render(
  <HashRouter>
    <App />
  </HashRouter>
  , entryNode);
