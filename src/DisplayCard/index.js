import React, { Component } from 'react';

import './styles.scss';
import RevealTile from '../RevealTile';

class DisplayCard extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="display-card">
        <h1>{this.props.title}</h1>
        <RevealTile className="display-img" title="firstCampus" description="very fun times..." animationName={this.props.animationName} />
        <p>{this.props.description}</p>
      </div>
    );
  }
}

DisplayCard.defaultProps = {
  title: 'displayTitle',
  description: 'displayDescription',
  imageUrl: 'http://www.kensap.org/wp-content/uploads/empty-photo.jpg',
  imageAlt: 'imageAlt',
};

export default DisplayCard;
