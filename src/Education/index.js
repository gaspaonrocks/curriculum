import React, { Component } from 'react';

import './styles.scss';

import DisplayCard from '../DisplayCard';

class Education extends Component {
  render() {
    const attr = {
      title: 'Établissement',
      description: 'Bloc de Description',
      imageAlt: 'campusPicture',
    };

    return (
      <div className="education-container">
        <DisplayCard title={attr.title} description={attr.description} imageAlt={attr.imageAlt} animationName='swing' />
        <DisplayCard title={attr.title} description={attr.description} imageAlt={attr.imageAlt} animationName='swing' />
        <DisplayCard title={attr.title} description={attr.description} imageAlt={attr.imageAlt} animationName='swing' />
      </div>
    );
  }
}

export default Education;
